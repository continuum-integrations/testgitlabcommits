#!/usr/bin/env python3

import sys
import inspect
import os
import subprocess
import getopt
from typing import Optional
from os.path import abspath
from pathlib import Path, PurePath
from random import randint, choice
from time import sleep

from lorem_text import lorem


BASE_DIR = Path(PurePath(abspath(inspect.getfile(inspect.currentframe()))).parent, "data")


def usage_and_exit(err: Optional[str] = None):
    stream = sys.stderr if err else sys.stdout
    if err:
        stream.write(f"ERROR: {err}\n\n")
    stream.write(f"""
Usage: {sys.argv[0]} [OPTIONS]

Options:

   -h, -?           Show this usage message
   --prefix=PRE     Use PRE as the story prefix [default: S]
   --start=N        Use N as the lower bound of story numbers [default: 90000]
   --end=N          Use N as the upper bound of story numbers [default: 90555]
   --count=N        Quit (less-than-relentlessly) after pushing N times
   --dry-run, -n    Dry-run mode (no actual commits or pushes)

""")
    sys.exit(err and 2 or 0)


def run_cmd(cmd, cwd):
    result = subprocess.run(cmd, check=True, stdout=subprocess.PIPE, cwd=cwd)
    result.check_returncode()


def edit_committable(rel_path: str):
    filename = Path(BASE_DIR, rel_path)
    os.makedirs(filename.parent, exist_ok=True)
    with open(filename, "a") as readme:
        readme.write(f"# {lorem.words(4).capitalize()}\n\n{lorem.paragraph()}\n\n")


def do_commits(story_prefix: str, range_start: int, range_end: int, dryrun_mode=False):
    story_num: int = randint(range_start, range_end)  
    filename: str = f"{story_prefix}/{story_num}.md"
    story_ref = f"{story_prefix}-{story_num}"
    num_commits = randint(1, 10)
    sys.stderr.write(f"Making {num_commits} commits...\n")
    for i in range(num_commits):
        msg: str = f"{story_ref}: {lorem.words(4).capitalize()}"
        print(msg)
        edit_committable(filename)
        if not dryrun_mode:
            run_cmd(["git", "add", filename], str(BASE_DIR))
            run_cmd(["git", "commit", "-m", msg], str(BASE_DIR))


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h?n", ["prefix=", "start=", "end=", "count=", "dry-run"])
    except getopt.GetoptError as e:
        usage_and_exit(str(e))

    story_prefix = "S"
    range_start = 90000
    range_end = 90555
    push_count = 0
    dryrun_mode = False

    try:
        if len(args):
            raise getopt.GetoptError(f"Got unexpecated arguments: {args}")
        for opt, arg in opts:
            if opt in ['-h', '-?']:
                usage_and_exit()
            elif opt in ["--prefix"]:
                story_prefix = arg
            elif opt in ["--start"]:
                range_start = int(arg)
            elif opt in ["--end"]:
                range_end = int(arg)
            elif opt in ["--count"]:
                push_count = int(arg)
            elif opt in ["--dry-run", "-n"]:
                dryrun_mode = True
            else:
                raise getopt.GetoptError(f"Unrecognized option: {opt}")
    except Exception as e:
        usage_and_exit(str(e))

    num_pushes: int = 0
    while True:
        do_commits(story_prefix, range_start, range_end, dryrun_mode)
        sys.stderr.write(f"Pushing commits...\n")
        if not dryrun_mode:
            run_cmd(["git", "push", "origin"], str(BASE_DIR))
            num_pushes += 1
            if push_count > 0 and num_pushes == push_count:
                break
        sleepy_time = randint(10, 20)
        sys.stderr.write(f"Sleeping for {sleepy_time}s...\n")
        sleep(sleepy_time)
