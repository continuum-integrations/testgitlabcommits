#!/usr/bin/env python3

import sys
import subprocess
from typing import Optional, List, Tuple
from dataclasses import dataclass
import logging
from k6ishared.platform_client import (
    ApiTokenPlatformAuthentication,
    PlatformClient,
)
from oceanus.common.config import OceanusConfig
from k6ishared.jsonservice import JSONService, Session, DIGITALAI_ACCOUNT_ID_HEADER

try:
    from pick import pick, Option
except Exception:
    print("The `pick` package is required for this script to work.")
    sys.exit(1)
try:
    from rich.console import Console
    from rich.padding import Padding
    from rich.panel import Panel
    from rich.prompt import Prompt, Confirm
    from rich.syntax import Syntax
    from rich.text import Text, TextType
except Exception:
    print("The `rich` package is required for this script to work.")
    sys.exit(1)

from k6ishared.jsonservice import JSONService
deep_get = JSONService.deep_get


class MyConfig(OceanusConfig):
    @property
    def platform_url(self):
        return self.get_env_var("DAI_PLATFORM_URL", "http://localhost:9000")

    @property
    def platform_token(self):
        return self.get_env_var("DAI_PLATFORM_TOKEN", "DEV_TOKEN_ONLY")

    @property
    def insight_url(self):
        return self.get_env_var("DAI_INSIGHT_APIURL", self.platform_url)

    @property
    def domain_url(self):
        return self.get_env_var("DAI_DOMAIN_APIURL", self.platform_url)



class InsightServicesClient(JSONService):
    def __init__(self) -> None:
        self.logger = logging.getLogger(f"{self.__module__}.{self.__class__.__qualname__}")
        cfg = MyConfig()
        session: Optional[Session] = None
        platform_auth = ApiTokenPlatformAuthentication(cfg.platform_token)
        platform_client = PlatformClient(
            platform_url=cfg.platform_url, platform_auth=platform_auth
        )
        session = platform_client.get_session()
        super().__init__(
            base_url=cfg.insight_url,
            verify=cfg.verify_ssl_on_thirdparty,
            session=session,
        )

    def get_applications(self) -> List[dict]:
        resp: dict = self.get("/insight/applications/")
        return resp.get("results", [])

    def get_progression(self, id: str) -> List[dict]:
        resp: dict = self.get(f"/insight/progressions/{id}")
        return resp
    
    def create_revision(
        self,
        package_id: str,
        repository_id: str,
        branch: str,
        progression_id: str,
        phase_id: str,
        version: str,
    ) -> dict:
        _data = {
            "version": version,
            "repository_id": repository_id,
            "branch": branch,
            "progression_id": progression_id,
            "phase_id": phase_id,
        }
        resp: dict = self.post(f"/insight/packages/{package_id}/revisions/", data=_data)
        return resp


class DomainServicesClient(JSONService):
    def __init__(self) -> None:
        self.logger = logging.getLogger(f"{self.__module__}.{self.__class__.__qualname__}")
        cfg = MyConfig()
        session: Optional[Session] = None
        platform_auth = ApiTokenPlatformAuthentication(cfg.platform_token)
        platform_client = PlatformClient(
            platform_url=cfg.platform_url, platform_auth=platform_auth
        )
        session = platform_client.get_session()
        super().__init__(
            base_url=cfg.domain_url,
            verify=cfg.verify_ssl_on_thirdparty,
            session=session,
        )

    def get_correlation_config(self, id: str) -> dict:
        resp = self.get(f"/configuration/items/{id}")
        return resp
    
    def get_package(self, id: str) -> dict:
        resp = self.get(f"/domain/packages/{id}")
        return resp

    def get_repository(self, id: str) -> dict:
        resp = self.get(f"/domain/repositories/{id}")
        return resp


class InsightDemoConsole(Console):
    def header(self, title):
        self.print(f"[green]{title}[/green]", style="bold")

    def subheader(self, text: str, title=""):
        self.print(Padding(Panel.fit(text, title=title, style="green"), (1, 0)))

    def info(self, text: str, title="Info"):
        self.print(Padding(Panel.fit(text, title=title, style="steel_blue1 on navy_blue"), (1, 0)))

    def warn(self, text: str, title="Warning"):
        self.print(Padding(Panel.fit(text, title=title, style="white on red"), (1, 0)))

    def abort(self, text: str = "Action canceled by user."):
        self.print(Padding(Panel.fit(text, style="black on gold1"), (1, 0)))
        sys.exit(1)


class InsightDemoConfirm(Confirm):
    @classmethod
    def ask(cls, *args, **kwargs):
        try:
            return super().ask(*args, **kwargs)
        except KeyboardInterrupt:
            console = InsightDemoConsole()
            console.abort()


class InsightDemoPrompt(Prompt):
    @classmethod
    def ask(cls, *args, **kwargs):
        try:
            return super().ask(*args, **kwargs)
        except KeyboardInterrupt:
            console = InsightDemoConsole()
            console.abort()

    @classmethod
    def ask_pw(cls, prompt: TextType = "", *args, **kwargs):
        kwargs["password"] = True
        kwargs["show_default"] = False
        if kwargs.get("default"):
            _prompt = (
                Text.from_markup(prompt, style="prompt") if isinstance(prompt, str) else prompt
            )
            _prompt.append(" ")
            _prompt.append(Text("(*****)", "prompt.default"))

        return cls.ask(_prompt, *args, **kwargs)


@dataclass
class ApplicationOption:
    package_name: str
    package_id: str
    repository_id: str
    repository_name: str
    progression_id: str


def select_application(insight_api: InsightServicesClient, domain_api: DomainServicesClient):
    options = []
    for application in insight_api.get_applications():
        if not "correlation_configuration_id" in application:
            continue
        if not "progression_id" in application:
            continue
        correlation = domain_api.get_correlation_config(application["correlation_configuration_id"])
        package_id = deep_get(correlation, ["data", "package_id"])
        repository_id = deep_get(correlation, ["data", "repository_id"])
        if not package_id and repository_id:
            continue
        package = domain_api.get_package(package_id)
        repository = domain_api.get_repository(repository_id)
        options.append(
            Option(
                package["name"],
                ApplicationOption(
                    package_name=package["name"],
                    package_id=package_id,
                    repository_id=repository_id,
                    repository_name=repository["full_name"],
                    progression_id=application["progression_id"],
                )
            )
        )
    if not options:
        raise RuntimeError("No Applications found")
    return pick(
        options,
        "Select an Application",
        multiselect=False,
        min_selection_count=1,
    )


def select_phase(insight_api: InsightServicesClient, progression_id: str):
    progression = insight_api.get_progression(progression_id)
    options = [Option(x["name"], x["id"]) for x in progression["phases"]]
    if not options:
        raise RuntimeError("No Phases found")
    return pick(
        options,
        "Select a Progression Phase",
        multiselect=False,
        min_selection_count=1,
    )


def run_cmd(cmd, cwd):
    return subprocess.run(cmd, capture_output=True, text=True).stdout


if __name__ == "__main__":

    insight_api = InsightServicesClient()
    domain_api = DomainServicesClient()

    console = InsightDemoConsole()
    console.print(
        Padding(
            Panel(
                "Create a New Package Revision",
                title="Insight Demo",
                subtitle="Digital.ai",
                style="green bold",
            ),
            (1, 0),
        )
    )

    while True:

        opt: Option
        idx: int

        opt, idx = select_application(insight_api, domain_api)
        application: ApplicationOption = opt.value

        opt, idx = select_phase(insight_api, application.progression_id)
        phase_id: str = opt.value

        this_branch = run_cmd(["git", "rev-parse", "--abbrev-ref", "HEAD"], ".").rstrip()
        branch = InsightDemoPrompt.ask(
            f"Which branch of {application.repository_name}?",
            default=(this_branch or "main"),
        )
        version = InsightDemoPrompt.ask(
            "Which is this revision's version?",
            default="build",
        )

        opt, idx = pick(
            [Option("yes", "yes"), Option("no", "no"), Option("quit", "quit")],
            "Create Package Revision?",
            multiselect=False,
            min_selection_count=1,
        )
        answer: str = opt.value

        if answer == "yes":
            try:
                insight_api.create_revision(
                    application.package_id,
                    application.repository_id,
                    branch,
                    application.progression_id,
                    phase_id,
                    version,
                )
            except Exception:
                console.print("Unable to create Package Revision.")
        elif answer == "quit":
            break

    console.print(Padding(Panel.fit("And thus concludes our time together.", style="yellow2"), (1, 0, 0, 0)))
    console.print()
